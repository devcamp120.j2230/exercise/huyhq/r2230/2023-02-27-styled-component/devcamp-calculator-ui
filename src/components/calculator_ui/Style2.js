import { BackgroundChild } from "../style/StyleBackground";
import { ButtonChildren } from "../style/StyleButton";
import { TextResultChildrend } from "../style/StyleTextResult";

const Style2 = () => {
    return (
        <BackgroundChild bgColor="#ffff" fontColor="#0000">
            <TextResultChildrend bgColor="#DADCE3">0</TextResultChildrend>
            <div>
                <ButtonChildren bgColor="#CED3D9" fontColor="#9EA3A4" width="200px">DEL</ButtonChildren>
                <ButtonChildren bgColor="#CED3D9" fontColor="#9EA3A4" width="100px">&lt;-</ButtonChildren>
                <ButtonChildren bgColor="#B9D7D5" fontColor="#9EA3A4" width="100px">/</ButtonChildren>
            </div>
            <div>
                <ButtonChildren bgColor="#E4E4EB" fontColor="#80ACAF" width="100px">7</ButtonChildren>
                <ButtonChildren bgColor="#E4E4EB" fontColor="#80ACAF" width="100px">8</ButtonChildren>
                <ButtonChildren bgColor="#E4E4EB" fontColor="#80ACAF" width="100px">9</ButtonChildren>
                <ButtonChildren bgColor="#B9D7D5" fontColor="#9EA3A4" width="100px">*</ButtonChildren>
            </div>
            <div>
                <ButtonChildren bgColor="#E4E4EB" fontColor="#80ACAF" width="100px">4</ButtonChildren>
                <ButtonChildren bgColor="#E4E4EB" fontColor="#80ACAF" width="100px">5</ButtonChildren>
                <ButtonChildren bgColor="#E4E4EB" fontColor="#80ACAF" width="100px">6</ButtonChildren>
                <ButtonChildren bgColor="#B9D7D5" fontColor="#9EA3A4" width="100px">-</ButtonChildren>
            </div>
            <div>
                <ButtonChildren bgColor="#E4E4EB" fontColor="#80ACAF" width="100px">1</ButtonChildren>
                <ButtonChildren bgColor="#E4E4EB" fontColor="#80ACAF" width="100px">2</ButtonChildren>
                <ButtonChildren bgColor="#E4E4EB" fontColor="#80ACAF" width="100px">3</ButtonChildren>
                <ButtonChildren bgColor="#B9D7D5" fontColor="#9EA3A4" width="100px">+</ButtonChildren>
            </div>
            <div>
                <ButtonChildren bgColor="#E4E4EB" fontColor="#80ACAF" width="100px">0</ButtonChildren>
                <ButtonChildren bgColor="#E4E4EB" fontColor="#80ACAF" width="100px">.</ButtonChildren>
                <ButtonChildren bgColor="#599E99" fontColor="#ffff" width="200px">=</ButtonChildren>
            </div>
        </BackgroundChild>
    );
}

export default Style2;