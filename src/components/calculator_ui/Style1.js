import { BackgroundChild } from "../style/StyleBackground";
import { ButtonChildren } from "../style/StyleButton";
import { TextResultChildrend } from "../style/StyleTextResult";

const Style1 = ()=>{
    return (
        <BackgroundChild bgColor="#3A4655" fontColor="#ffff">
            <TextResultChildrend fontColor="#717880" fontSize="40px">2536+419+</TextResultChildrend>
            <hr/>
            <TextResultChildrend>2955</TextResultChildrend>
            <div>
                <ButtonChildren fontColor="#D85C4E">C</ButtonChildren>
                <ButtonChildren>#</ButtonChildren>
                <ButtonChildren>%</ButtonChildren>
                <ButtonChildren>/</ButtonChildren>
            </div>
            <div>
                <ButtonChildren bgColor="#425062" fontColor="#E9F7FD">7</ButtonChildren>
                <ButtonChildren bgColor="#425062" fontColor="#E9F7FD">8</ButtonChildren>
                <ButtonChildren bgColor="#425062" fontColor="#E9F7FD">9</ButtonChildren>
                <ButtonChildren>x</ButtonChildren>
            </div>
            <div>
                <ButtonChildren bgColor="#425062" fontColor="#E9F7FD">4</ButtonChildren>
                <ButtonChildren bgColor="#425062" fontColor="#E9F7FD">5</ButtonChildren>
                <ButtonChildren bgColor="#425062" fontColor="#E9F7FD">6</ButtonChildren>
                <ButtonChildren>-</ButtonChildren>
            </div>
            <div>
                <ButtonChildren bgColor="#425062" fontColor="#E9F7FD">1</ButtonChildren>
                <ButtonChildren bgColor="#425062" fontColor="#E9F7FD">2</ButtonChildren>
                <ButtonChildren bgColor="#425062" fontColor="#E9F7FD">3</ButtonChildren>
                <ButtonChildren>+</ButtonChildren>
            </div>
            <div>
                <ButtonChildren bgColor="#425062" fontColor="#E9F7FD">.</ButtonChildren>
                <ButtonChildren bgColor="#425062" fontColor="#E9F7FD">0</ButtonChildren>
                <ButtonChildren bgColor="#425062" fontColor="#E9F7FD">&lt;</ButtonChildren>
                <ButtonChildren>=</ButtonChildren>
            </div>
        </BackgroundChild>
    );
}

export default Style1;