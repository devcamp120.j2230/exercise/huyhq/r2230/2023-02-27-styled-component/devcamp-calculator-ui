import styled from "styled-components";

const ButtonParent = styled.button`
        background: #404C5F;
        color: #AFB3BB;
        width: 100px;
        font-size: 40px;
        border: none;
        height: 100px;
        text-align: center;
        margin: 1px;
    `;

const ButtonChildren = styled(ButtonParent)`
        background: ${prop => prop.bgColor};
        color: ${prop => prop.fontColor};
        width: ${prop => prop.width};
    `
export {ButtonParent, ButtonChildren};