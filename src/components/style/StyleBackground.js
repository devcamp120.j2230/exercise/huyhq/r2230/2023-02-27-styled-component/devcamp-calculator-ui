import styled from "styled-components";

const BackgroundParent = styled.div`
        color: #ffff;
        background: #3A4655;
        max-width: fit-content;
        margin: 10px auto;
        padding: 5px;
        border:  5px #969999 solid;
    `
const BackgroundChild = styled(BackgroundParent)`
        color: ${prop => prop.fontColor};
        background: ${prop => prop.bgColor};
    `
export { BackgroundChild };