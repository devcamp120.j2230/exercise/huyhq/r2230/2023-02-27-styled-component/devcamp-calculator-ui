import Style1 from "./components/calculator_ui/Style1";
import Style2 from "./components/calculator_ui/Style2";

function App() {
  return (
    <div>
      <Style1/>
      <hr/>
      <Style2/>
    </div>
  );
}

export default App;
